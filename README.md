Date and Time Joomla Module
===========================

This module shows the current time and date.
https://ty2u.com/product/date-and-time-joomla-module/

Icon used on JED:
http://www.iconarchive.com/show/buuf-icons-by-mattahan/Time-Date-icon.html